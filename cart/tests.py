from django.test import TestCase, Client
from unittest.mock import Mock

from .forms import CartAddProductForm
from .context_processors import cart
from .cart import Cart
from shop.models import Category, Product


class CartAddProductFormTest(TestCase):

    def setUp(self):
        self.request = Mock()
        self.request.session = Client().session

    def test_cart_add_product_form(self):
        form = CartAddProductForm((1, 1))
        self.assertEqual(form.data, (1, 1))

    def test_context_processors_cart(self):
        c = cart(self.request)
        self.assertIsInstance(c['cart'], Cart)


class CartTest(TestCase):

    def setUp(self):
        self.request = Mock()
        self.request.session = Client().session
        self.cart = Cart(self.request)

        self.category = Category(name='test', slug='test')
        self.category.save()
        self.product = Product(category=self.category, name='puer',
                               slug='puer', price=10, stock=5)
        self.product.save()
        self.product2 = Product(category=self.category, name='black',
                                slug='black', price=5, stock=5)
        self.product2.save()

    def test_add_to_cart(self):
        print((self.request.session.modified))
        self.cart.add(self.product)
        self.assertEqual(len(self.cart), 1)

    def test_remove_to_cart(self):
        self.cart.add(self.product)
        self.cart.add(self.product2)
        self.cart.remove(self.product)
        self.assertEqual(len(self.cart), 1)

    def test_get_total_price_cart(self):
        self.cart.add(self.product)
        self.cart.add(self.product2)
        total_price = self.cart.get_total_price()
        self.assertEqual(total_price, 15)

    def test_iter_cart(self):
        self.cart.add(self.product)
        self.cart.add(self.product2)
        v = list(self.cart.__iter__())
        self.assertEqual(len(v), 2)

    def test_get_discount_cart(self):
        discount = self.cart.get_discount()
        self.assertEqual(discount, 0)

    def test_get_total_price_discount_cart(self):
        self.cart.add(self.product)
        self.cart.add(self.product2)
        total_price = self.cart.get_total_price_discount()
        self.assertEqual(total_price, 15)
