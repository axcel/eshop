from django.test import TestCase
from django.core.urlresolvers import reverse

from .models import Product, Category


class ShopProductViews(TestCase):
    def setUp(self):
        self.category = Category(name='test', slug='test')
        self.category.save()
        self.product = Product(category=self.category, name='puer',
                               slug='puer', price=10, stock=5)
        self.product.save()

    def test_product_list_view(self):
        response = self.client.get(reverse("shop:product_list"))
        self.assertEqual(response.status_code, 200)

    def test_product_list_by_category_view(self):
        response = self.client.get(reverse("shop:product_list_by_category",
                                           args=[self.category.slug]))
        self.assertEqual(response.status_code, 200)

    def test_product_detail_view(self):
        response = self.client.get(reverse("shop:product_detail",
                                           args=[self.product.id,
                                                 self.product.slug]))
        self.assertEqual(response.status_code, 200)
